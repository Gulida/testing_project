import os
import time

import pytest
from selenium.common import NoSuchElementException, TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from common.common_locators import CommonLocators


class Base():
    def __init__(self, browser, url, timeout=10, test_name=""):
        self.browser = browser
        self.url = url
        self.wait = WebDriverWait(browser, timeout)
        self.timeout = timeout
        self.test_name = test_name

    def open(self):
        self.browser.get(self.url)

    def sleep(self, second):
        time.sleep(second)

    def is_element_present(self, how, what, element=None):
        try:
            if (element is None):
                self.browser.find_element(how, what)
            else:
                element.find_element(how, what)
        except NoSuchElementException:
            return False
        return True

    def find_element(self, how, what):
        self.sleep(1)
        element = self.browser.find_element(how, what)
        if element == None:
            return False
        return element

    def find_elements(self, locator: tuple, parent_element=None, fail_if_not_exist=False):
        if parent_element is None:
            parent_element = self.browser
        try:
            elements = parent_element.find_elements(*locator)
        except:
            if fail_if_not_exist:
                self.fail("There is no elements - '{}'".format(locator))
            elements = False
        return elements

    def wait_element(self, locator:tuple, parent_element=None, timeout=None, fail_if_not_exist=False):
        """
        Ждет пока элемент появиться на странице, и возвращает элемент
        Если не найдет до конца self.timeout возвращает False, можно задать свой таймаут
        """
        print('LOCATOR: ', locator)
        try:
            if (parent_element == None):
                parent_element = self.browser
            if (timeout == None):
                timeout = self.timeout
            wait = WebDriverWait(parent_element, timeout)
            element = wait.until(EC.presence_of_element_located(locator))
            return element
        except TimeoutException:
            if fail_if_not_exist:
                self.fail('Could not wait element - "{}"'.format(locator))
            return False
        except:
            self.fail("Something happened while wating")

    def wait_elements(self, locator:tuple, parent_element=None):
        """
        Ждет пока хотябы один элемент появиться на странице,
        После того как нашел один элемент, сразу же ищет с такими локаторами все элементы и возвращает их.
        Если не найдет до конца self.timout ни одного элемента возвращат False
        """
        try:
            if (parent_element is None):
                elements = self.wait.until(EC.presence_of_all_elements_located(locator))
            else:
                wait = WebDriverWait(parent_element, self.timeout)
                elements = wait.until(EC.presence_of_all_elements_located(locator))
        except:
            return False
        return elements

    def wait_to_clickable(self, how, what):
        """
        Ждет пока элемент не станет кликабельным,
        Если не станет возвращает False
        """
        try:
            self.wait.until(EC.element_to_be_clickable((how, what)))

        except TimeoutException:
            return False
        return True

    def fail(self, message, screenshot_name='default_fail'):
        """
        Вызывает ошибку, и останавливает тест. Второй аргумент название скриншота.
        Фото хранится в папке images
        """
        counter = 0
        for (dirpath, dirnames, filenames) in os.walk(os.path.abspath(os.getcwd()) + '\\Images\\fails\\'):
            counter = str(len(filenames) + 1)
            break

        with open('images/console_logs/' + screenshot_name + '_' + str(counter) + '.log', 'w') as f:
            logs = ''
            for entry in self.browser.get_log('browser'):
                logs += entry['level'] + '    ------     ' + entry['message'] + '\n'
            f.write(logs)

        self.browser.save_screenshot('images/fails/' + screenshot_name + '_' + str(counter) + '.png')
        pytest.fail(message + ',     screenshot - ' + screenshot_name + '_' + str(counter) + '.png')

    def wait_for_loader_invisibility(self):
        """
        Ждет пока лоадер исчезнет
        """
        time.sleep(1)
        loaders = self.find_elements(CommonLocators.LOADER)
        for loader in loaders:
            try:
                WebDriverWait(self.browser, self.timeout).until(EC.invisibility_of_element(loader))
            except TimeoutException as ex:
                self.fail("Loader stuck !!!", 'loader_timeout_exception')
            except:
                self.fail("Something happened to the loader", 'loader_exception')
