from base.base import Base


class GetAttribute(Base):
    def get_text(self, how, what, parent_element=None):
        element = self.wait_element((how, what), parent_element)
        if element == False:
            return False

        return element.text
