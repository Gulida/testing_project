from selenium.webdriver.support.wait import WebDriverWait

from components.scroll_to_element import ScrollToElement
from selenium.webdriver.support import expected_conditions as EC


class Button(ScrollToElement):
    def click_button(self, how, what, btn, element=None):
        """
        :param: Receives locator and button name / Получает локатор и название кнопки
        :return: Clicks the button / Нажимает кнопку
        """
        if self.is_element_present(how, what, element=element):
            # self.scroll_to_component(how, what, element=element)
            self.scroll_to_component(how, what, element=element)
            if element is not None:
                wait = WebDriverWait(element, self.timeout)
                button = wait.until(EC.element_to_be_clickable((how, what)))
                try:
                    button.click()
                except:
                    try:
                        self.sleep(1)
                        self.scroll_to_component(how, what, element=element)
                        button.click()
                    except:
                        self.fail("Could not click on - " + btn)
            else:
                button = self.wait.until(EC.element_to_be_clickable((how, what)))
                button.click()
            print(f'{btn} - button was pressed...')
            return True
        else:
            print(f'{btn} button is not visible!!! locator - "({how}, {what})"')
            return False
