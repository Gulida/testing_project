from base.base import Base


class ScrollToElement(Base):
    def scroll_to_component(self, how, what, element=None):
        if element is None:
            element = self.browser
        elemen = element.find_element(how, what)
        coordinates = elemen.location_once_scrolled_into_view  # returns dict of X, Y coordinates
        # print('COORDINATES: ', coordinates)
        self.browser.execute_script('window.scrollTo({}, {});'.format(coordinates['x'], coordinates['y']))
        self.sleep(1)