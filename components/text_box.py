from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from base.base import Base


class TextBox(Base):
    def set_textbox_value_wait(self, how, what, text):
        print('SET TEXT BOX')
        text_box = self.find_element(how, what)
        if text_box == False:
            print('ONE')
            return False
        if text_box.get_property('disabled'):
            print('TWO')
            return False
        print('TRHEE')
        self.wait_to_clickable(how, what)
        text_box.send_keys(Keys.CONTROL, "a")
        text_box.send_keys(text)
        return True
