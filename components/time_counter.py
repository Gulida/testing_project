
import warnings

import pytest

from components.bcolors import BColors


def running_time_counter(start, end, method_name):
    if (end - start) < 5:
        print(f'\n{BColors.RESET}{method_name}\n{BColors.OK}Running time of script: {end - start}. The script is running normal!')

    elif (end - start) < 10:
        print(f'\n{BColors.RESET}{method_name}\n{BColors.WARNING}Running time of script: {end - start}. !!! Warning: The script '
              f'is running normal!')
        warnings.warn(f'\n{method_name}\nRunning time of script: {end - start}. !!! Warning: The script is running normal!')

    else:
        print(f'\n{BColors.RESET}{method_name}\n{BColors.FAIL}Running time of script: {end - start}. !!! Run time fail: The '
              f'script runs too long!')
        pytest.fail(f'Running time of script: {end - start}. !!! Run time fail: The scr/ipt runs too long!')

