from base.base import Base
from common.common_data import CommonVariables
from common.common_locators import CommonLocators


class AlertDialog(Base):
    def check_error_text(self, error_type='', custom_error_message=''):
        """
        Принимает тип ошибки или кастомный текст ошибки,
        и возвращает true если текст ошибки совподает с ошибкой на alertDialog
        Все типы ошибок можно посмотреть в CommonVariables
        """
        if (error_type != ''):
            message = self.check_translated_message(error_type)
        elif (custom_error_message == ''):
            message = custom_error_message
        else:
            message = None
            print("You are using wrong way to check an error!")

        alert_text = self.get_alert_text()

        return message == alert_text

    def check_translated_message(self, error_type):
        """
        Принимает тип ошибки и возвращает локализованную строку ошибки
        """
        # language = self.get_system_language()
        language = 'En'

        return CommonVariables.alert_msg[language][error_type]


    def get_alert_text(self):
        """
        Вовращает текст алерта
        """
        alert_text = ''
        if self.check_alert_visibility():
            alert_text = self.browser.find_element(
                *CommonLocators.ALERT_MESSAGE).text
        return alert_text

    def check_alert_visibility(self):
        """
        Проверяет страницу на алерт ошибки
        Если вышел алерт с ошибкой возвращает true, если нет false
        """
        return self.is_element_present(*CommonLocators.ALERT_MESSAGE)
