from time import process_time
import time

from selenium.webdriver.common.by import By

from components.get_attribute import GetAttribute
from components.time_counter import running_time_counter
from pages.main_page import MainPage


class TestAuthorizationPage(MainPage, GetAttribute):
    def test(self):
        start = process_time()
        self.open()
        self.sleep(5)

        credencails = self.get_text(By.ID, 'login_credentials').split('\n')
        print('ELEMENT: ', credencails)
        password = self.get_text(By.CLASS_NAME, 'login_password').split('\n')
        print('Password - ', password)

        self.test_authorization({'username': credencails[1], 'password': password[1]})
        self.sleep(5)

        # self.set_textbox_value_wait(By.ID, 'user-name', credencails[1])
        # self.sleep(2)
        # self.set_textbox_value_wait(By.ID, 'password', password[1])
        # self.sleep(2)
        # self.click_button(By.ID, 'login-button', 'LOGIN')
        # self.sleep(3)

        # for i in range(1, len(credencails)):


        end = process_time()
        # Подсчет времени процесса / Process time counting
        running_time_counter(start, end, ' Test ENDS')


