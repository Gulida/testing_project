from selenium.webdriver.common.by import By


class CommonLocators():
    # Alert dialog label_locators
    ALERT_MESSAGE = (By.NAME, 'AlertDialogMessage')
    ALERT_OK_BUTTON = (By.NAME, 'AlertButtonOk')
    ALERT_ACCEPT_BUTTON = (By.NAME, 'AlertButtonYes')
    ALERT_CANCEL_BUTTON = (By.NAME, 'AlertButtonNo')

    # Login page locators
    AUTH_FORM = (By.ID, 'login_button_container')
    USERNAME = (By.ID, 'user-name')
    PASSWORD = (By.ID, 'password')
    SIGN_IN_BUTTON = (By.ID, 'login-button')

    SIGN_OUT_BUTTON = (By.ID, 'logoutButton')
    LOGO = (By.ID, 'LogoButton')

    # Loader
    LOADER = (By.NAME, 'LoaderBackDrop')