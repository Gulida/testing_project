class CommonVariables():
    alert_msg = {
        'En': {
            'required_field': 'Please, fill in all the fields correctly!',
            'exist_data': 'Such data exist',
            'exist_data_message_for_add': 'You are trying to add almost existing data...',
            'exist_data_message_for_edit': 'Cannot be renamed to this value',
            'unable_to_delete': 'Unable to delete',
            'unable_to_delete_message': 'Unable to delete',
            'accept_delete': "Are you sure?",
            'accept_close_cash_box': 'Do you really want to close the cashbox?'
        },
        'Ru': {
            'required_field': 'Пожалуйста, заполните все поля правильно!',
            'exist_data': 'Такие данные существуют',
            'exist_data_message_for_add': 'Вы пытаетесь добавить уже существующие данные...',
            'exist_data_message_for_edit': 'Не может быть переименован в это значение',
            'unable_to_delete': 'Невозможно удаление',
            'unable_to_delete_message': 'Невозможно удаление',
            'accept_delete': "Вы уверены?",
            'accept_close_cash_box': 'Вы действительно хотите закрыть кассу'
        }
    }

    valid_msg = {
        'Ru': {
            'fieldRequired': 'Поле обязательно для заполнения',
            'chooseRightDateStart': 'Выберите правильную дату',
            'expiredDate': 'Срок действия истёк',
            'onlyDigits': 'Введите только цифры',
            'onlyText': 'Введите только буквы',
            'emailAddress': 'Введите правильный email адрес',
            'lengthMustBe': 'Длина должна быть',
            'passwordMismatch': 'Пароли не совпадают',
            'notLess': 'Не меньше',
            'notMore': 'Не больше',
            'maxLength': 'МАКСИМАЛЬНАЯ ДЛИНА',
            'minLength': 'МИНИМАЛЬНАЯ ДЛИНА',
            'ageShouldBe': 'Должен быть старше',
        },
        'En': {
            'fieldRequired': 'Required field',
            'chooseRightDateStart': 'Choose correct date',
            'expiredDate': 'Validity has expired',
            'onlyDigits': 'Enter only digits',
            'onlyText': 'Enter only letters',
            'emailAddress': 'Please enter a valid email address',
            'lengthMustBe': 'The length should be',
            'passwordMismatch': 'Password mismatch',
            'notLess': 'Not less',
            'notMore': 'Not more',
            'maxLength': 'Maximum length',
            'minLength': 'Minimum length',
            'ageShouldBe': 'Age must be over',
        }
    }
    # auth_info = {
    #     'email': 'admin',
    #     'password': 'Pa$$word123'
    # }

    add_new_cashbox_look_up = {
        'Ru': 'Создать новую кассу',
        'En': 'Create new cashbox'
    }

    system_language = {
        'ru': 'Ru',
        'en': 'En'
    }