from common.common_locators import CommonLocators
from components.button import Button
from components.text_box import TextBox


class Authorization(TextBox, Button):
    def fill_in_auth_form(self, auth_info):
        """
        :param auth_info: receives the data as: {'email': 'email', 'password': 'password'} / получает данные в виде {'email': 'email', 'password': 'password'}
        :return: logging in / авторизация в систему
        """

        print('IN AUTH PAGE')
        auth_form = self.wait_element(CommonLocators.AUTH_FORM)
        if auth_form == False:
            print('Authorization form is invisible')
        else:
            print('ELSE IN FILL AUTH PAGE')
            self.sleep(2)
            t = self.set_textbox_value_wait(*CommonLocators.USERNAME, auth_info['username'], 'USERNAME')
            if(t == False):
                self.fail("Something went wrong with username field")
            t = self.set_textbox_value_wait(*CommonLocators.PASSWORD, auth_info['password'], 'PASSWORD')
            if(t == False):
                self.fail("Something went wrong with password field")
            t = self.click_button(*CommonLocators.SIGN_IN_BUTTON, 'SIGN IN')
            if(t == False):
                self.fail("Something went wrong with clicking LOGIN button.")